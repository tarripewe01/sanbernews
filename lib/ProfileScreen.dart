import 'dart:ui';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sanber_news/Services/AuthenticationService.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:sanber_news/Utils/Colors.dart';

import 'SignInScreen.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    final firebaseUser = context.watch<User>();

    if (firebaseUser != null) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: myColors[PRIMARY],
          title: Text("My Profile"),
          elevation: 0,
        ),
        body: ListView(
          children: <Widget>[
            Container(
              color: myColors[PRIMARY],
              height: MediaQuery.of(context).size.height * 0.3,
              width: double.infinity,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 50,
                  ),
                  CircleAvatar(
                    radius: 50,
                    backgroundImage: NetworkImage(
                        "https://media-exp1.licdn.com/dms/image/C5603AQGTTL4UaBvCTw/profile-displayphoto-shrink_200_200/0/1615887693017?e=1622678400&v=beta&t=mTfo7JhJheMf9cedWqbJIUGoFFWjv4je1tbFhN3ZbU4"),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Tarri Peritha Westi",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                  ),
                  SizedBox(height: 5),
                  Text(
                    "Tanjung Pinang, Kepulauan Riau",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                  )
                ],
              ),
            ),
            Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          "Language",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 14),
                        ),
                        SizedBox(height: 5),
                        Text("Dart")
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          "Framework",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 14),
                        ),
                        SizedBox(height: 5),
                        Text("Flutter")
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          "Tools",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 14),
                        ),
                        SizedBox(height: 5),
                        Text("Figma")
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 10),
            Card(
              elevation: 5,
              child: Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 10),
                    Text(
                      "User Information",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                    Divider(),
                    ListTile(
                      title: Text("Location"),
                      subtitle: Text("Tanjung Pinang, Kepulauan Riau"),
                      leading: Icon(Icons.location_on),
                    ),
                    ListTile(
                      title: Text("Email"),
                      subtitle: Text("tarripewe01@gmail.com"),
                      leading: Icon(Icons.email),
                    ),
                    ListTile(
                      title: Text("Gitlab"),
                      subtitle: Text("@tarripewe01"),
                      leading: Icon(Icons.ac_unit),
                    ),
                    ListTile(
                      title: Text("About"),
                      subtitle:
                          Text("I'm currently learning Flutter at Sanbercode"),
                      leading: Icon(Icons.info),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    }
    return SignInScreen();
  }
}
