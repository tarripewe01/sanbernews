import 'package:flutter/material.dart';
import 'package:sanber_news/ArticleDetailScreen.dart';
import 'package:sanber_news/Utils/Colors.dart';
import 'Models/article_model.dart';

Widget customListTile(Article article, BuildContext context) {
  return InkWell(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ArticleDetailScreen(
                    article: article,
                  )));
    },
    child: Container(
      margin: EdgeInsets.all(5.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
          color: myColors[PRIMARY],
          borderRadius: BorderRadius.circular(12.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3.0,
            ),
          ]),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 200.0,
            width: double.infinity,
            decoration: BoxDecoration(
              //let's add the height

              image: DecorationImage(
                  image: NetworkImage(article.urlToImage), fit: BoxFit.cover),
              borderRadius: BorderRadius.circular(12.0),
            ),
          ),
          SizedBox(
            height: 8.0,
          ),
          Container(
            padding: EdgeInsets.all(6.0),
            decoration: BoxDecoration(
              color: myColors[WHITE],
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Text(
              article.source.name,
              style: TextStyle(
                color: myColors[TEXT],
              ),
            ),
          ),
          SizedBox(
            height: 8.0,
          ),
          Text(
            article.title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          )
        ],
      ),
    ),
  );
}

// return Card(
//   elevation: 2.0,
//   margin: EdgeInsets.only(bottom: 20.0),
//   child: Padding(
//     padding: EdgeInsets.all(8.0),
//     child: Row(
//       children: [
//         Container(
//           width: 80.0,
//           height: 80.0,
//           decoration: BoxDecoration(
//               image: DecorationImage(
//                 image: NetworkImage(article.urlToImage),
//                 fit: BoxFit.cover,
//               ),
//               borderRadius: BorderRadius.circular(8.0)),
//         ),
//         SizedBox(
//           width: 5.0,
//         ),
//         Expanded(
//             child: Column(
//           mainAxisAlignment: MainAxisAlignment.start,
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text(
//               article.title,
//               style: TextStyle(fontSize: 8.0),
//             ),
//             SizedBox(
//               height: 5.0,
//             ),
//             Row(
//               children: [
//                 Icon(Icons.person),
//                 Text(
//                   article.author,
//                   style: TextStyle(fontSize: 12.0),
//                 ),
//                 SizedBox(
//                   height: 10.0,
//                 ),
//                 Icon(Icons.date_range),
//                 Text(
//                   article.publishedAt,
//                   style: TextStyle(fontSize: 12.0),
//                 ),
//               ],
//             ),
//           ],
//         ))
//       ],
//     ),
//   ),
// );
