import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sanber_news/HomeScreen.dart';
import 'package:sanber_news/Services/AuthenticationService.dart';
import 'package:provider/provider.dart';
import 'package:sanber_news/SignUpScreen.dart';
import 'package:sanber_news/Utils/Colors.dart';
import 'mixins/validation.dart';
import 'package:form_field_validator/form_field_validator.dart';

import 'SignInScreen.dart';

class SignUpScreen extends StatefulWidget {
  SignUpScreen({Key key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();

  String email = "";
  String password = "";

  final TextEditingController emailController = TextEditingController();

  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final firebaseUser = context.watch<User>();
    return Scaffold(
      backgroundColor: myColors[PRIMARY],
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      child: Container(
                        margin: EdgeInsets.only(top: 50),
                        child: Center(
                          child: Text(
                            "REGISTER",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 40,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 30),
              Padding(
                padding: EdgeInsets.all(30.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: myColors[PRIMARY],
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                blurRadius: 20.0,
                                offset: Offset(0, 10))
                          ]),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            emailField(),
                            passwordField(),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 30),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: LinearGradient(colors: [
                            Color.fromRGBO(255, 255, 255, 1),
                            Color.fromRGBO(255, 255, 255, .6)
                          ])),
                      child: Center(
                        child: FlatButton(
                          onPressed: () {
                            context.read<AuthenticationService>().signUp(
                                  email: emailController.text.trim(),
                                  password: passwordController.text.trim(),
                                );
                            if (firebaseUser != null) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SignInScreen()));
                            }
                            // else {
                            //   var message;
                            //   showDialog(
                            //       context: context,
                            //       builder: (context) => AlertDialog(
                            //             title: Text('Error'),
                            //             // content: Text(e.message),
                            //             actions: <Widget>[
                            //               FlatButton(
                            //                 onPressed: () {
                            //                   Navigator.pop(context);
                            //                 },
                            //                 child: Text('OK'),
                            //               )
                            //             ],
                            //           ));
                            // }
                          },
                          child: Text(
                            "REGISTER",
                            style: TextStyle(
                                color: myColors[PRIMARY],
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 30),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: LinearGradient(colors: [
                            Color.fromRGBO(255, 255, 255, 1),
                            Color.fromRGBO(255, 255, 255, .6)
                          ])),
                      child: Center(
                        child: FlatButton(
                          onPressed: () {
                            Route route = MaterialPageRoute(
                                builder: (context) => SignInScreen());
                            Navigator.push(context, route);
                          },
                          child: Text(
                            "LOGIN",
                            style: TextStyle(
                                color: myColors[PRIMARY],
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    // FlatButton(
                    //   onPressed: () {},
                    //   child: Text(
                    //     "Forgot Password?",
                    //     style: TextStyle(
                    //       color: Colors.white,
                    //       fontWeight: FontWeight.bold,
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container passwordField() {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: TextFormField(
        controller: passwordController,
        obscureText: true,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Password",
            hintStyle: TextStyle(color: Colors.white)),
      ),
    );
  }

  Container emailField() {
    return Container(
      padding: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.white))),
      child: TextFormField(
        controller: emailController,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Email",
          hintStyle: TextStyle(color: Colors.white),
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
      ),
    );
  }
}
