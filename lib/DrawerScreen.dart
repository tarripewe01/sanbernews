import 'dart:ui';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:sanber_news/HomeScreen.dart';
import 'package:sanber_news/ProfileScreen.dart';
import 'package:sanber_news/Services/AuthenticationService.dart';
import 'package:provider/provider.dart';
import 'package:sanber_news/SettingsScreen.dart';
import 'package:sanber_news/Utils/Colors.dart';

class DrawerScreen extends StatefulWidget {
  DrawerScreen({Key key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  double value = 0;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text("Tarri Peritha Westi"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(
                  "https://media-exp1.licdn.com/dms/image/C5603AQGTTL4UaBvCTw/profile-displayphoto-shrink_200_200/0/1615887693017?e=1622678400&v=beta&t=mTfo7JhJheMf9cedWqbJIUGoFFWjv4je1tbFhN3ZbU4"),
            ),
            accountEmail: Text("tarripewe01@gmail.com"),
            decoration: BoxDecoration(
              color: myColors[PRIMARY],
            ),
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onTilePressed: () {
              Route route =
                  MaterialPageRoute(builder: (context) => HomeScreen());
              Navigator.push(context, route);
            },
          ),
          DrawerListTile(
            iconData: Icons.person,
            title: "Profile",
            onTilePressed: () {
              Route route =
                  MaterialPageRoute(builder: (context) => ProfileScreen());
              Navigator.push(context, route);
            },
          ),
          DrawerListTile(
            iconData: Icons.settings,
            title: "Settings",
            onTilePressed: () {
              Route route =
                  MaterialPageRoute(builder: (context) => SettingScreen());
              Navigator.push(context, route);
            },
          ),
          DrawerListTile(
            iconData: Icons.logout,
            title: "Log out",
            onTilePressed: () {
              context.read<AuthenticationService>().signOut();
            },
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
//     return Scaffold(
//       body: Stack(
//         children: [
//           Container(
//             decoration: BoxDecoration(
//               color: myColors[PRIMARY],
//             ),
//           ),
//           SafeArea(
//             child: Container(
//               width: 200,
//               padding: EdgeInsets.all(8.0),
//               child: Column(
//                 children: [
//                   DrawerHeader(
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         CircleAvatar(
//                           radius: 50.0,
//                           backgroundImage: NetworkImage(
//                               "https://media-exp1.licdn.com/dms/image/C5603AQGTTL4UaBvCTw/profile-displayphoto-shrink_200_200/0/1615887693017?e=1622678400&v=beta&t=mTfo7JhJheMf9cedWqbJIUGoFFWjv4je1tbFhN3ZbU4"),
//                         ),
//                         SizedBox(height: 10.0),
//                         Text(
//                           "Tarri Peritha Westi",
//                           style: TextStyle(color: Colors.white, fontSize: 18.0),
//                         ),
//                       ],
//                     ),
//                   ),
//                   Expanded(
//                     child: ListView(
//                       children: [
//                         ListTile(
//                           onTap: () {},
//                           leading: Icon(
//                             Icons.home,
//                             color: Colors.white,
//                           ),
//                           title: Text(
//                             "Home",
//                             style: TextStyle(color: Colors.white),
//                           ),
//                         ),
//                         ListTile(
//                           onTap: () {},
//                           leading: Icon(
//                             Icons.person,
//                             color: Colors.white,
//                           ),
//                           title: Text(
//                             "Profile",
//                             style: TextStyle(color: Colors.white),
//                           ),
//                         ),
//                         ListTile(
//                           onTap: () {},
//                           leading: Icon(
//                             Icons.settings,
//                             color: Colors.white,
//                           ),
//                           title: Text(
//                             "Settings",
//                             style: TextStyle(color: Colors.white),
//                           ),
//                         ),
//                         ListTile(
//                           onTap: () {},
//                           leading: Icon(
//                             Icons.logout,
//                             color: Colors.white,
//                           ),
//                           title: Text(
//                             "Log out",
//                             style: TextStyle(color: Colors.white),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//           TweenAnimationBuilder(
//               tween: Tween<double>(begin: 0, end: value),
//               duration: Duration(milliseconds: 500),
//               builder: (_, double val, __) {
//                 return (Transform(
//                   transform: Matrix4.identity()
//                     ..setEntry(3, 2, 0.001)
//                     ..setEntry(0, 3, 200 * val)
//                     ..rotateY((pi / 6) * val),
//                 ));
//               }),
//         ],
//       ),
//     );
//   }
// }
