import 'package:flutter/material.dart';


const PRIMARY = "primary";
const SECONDARY = "secondary";
const TEXT = "text";
const WHITE = "white";
const BODY = "body";

const Map<String, Color> myColors = {
  PRIMARY: Color.fromRGBO(8, 217, 214, 1),
  BODY: Color.fromRGBO(227, 253, 253, 1),
  SECONDARY: Color.fromRGBO(234, 234, 234, 1),
  TEXT: Colors.black,
  WHITE: Colors.white,
};