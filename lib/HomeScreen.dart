import 'package:flutter/material.dart';
import 'package:sanber_news/DrawerScreen.dart';
import 'package:sanber_news/Services/api_service.dart';
import 'package:sanber_news/Services/api_service_2.dart';
import 'package:sanber_news/Services/api_service_3.dart';
import 'package:sanber_news/Services/api_service_4.dart';
import 'package:sanber_news/Services/api_service_5.dart';
import 'package:sanber_news/Services/api_service_6.dart';
import 'package:sanber_news/customListTile.dart';

import 'Models/article_model.dart';
import 'Utils/Colors.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  List<Tab> _tabList = [
    Tab(
      child: Text("Top"),
    ),
    Tab(
      child: Text("Bussiness"),
    ),
    Tab(
      child: Text("Entertainment"),
    ),
    Tab(
      child: Text("Health"),
    ),
    Tab(
      child: Text("Science"),
    ),
    Tab(
      child: Text("Technology"),
    ),
  ];

  TabController _tabController;

  ApiService client = ApiService();
  ApiService2 client2 = ApiService2();
  ApiService3 client3 = ApiService3();
  ApiService4 client4 = ApiService4();
  ApiService5 client5 = ApiService5();
  ApiService6 client6 = ApiService6();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: _tabList.length, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 110.0,
        backgroundColor: myColors[PRIMARY],
        centerTitle: true,
        title: Text.rich(
          TextSpan(
              text: "sanber",
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                  color: Colors.black),
              children: [
                TextSpan(
                    text: "News",
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.white))
              ]),
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: TabBar(
            indicatorColor: myColors[PRIMARY],
            labelColor: myColors[TEXT],
            isScrollable: true,
            controller: _tabController,
            tabs: _tabList,
          ),
        ),
      ),
      drawer: DrawerScreen(),
      body: TabBarView(
        controller: _tabController,
        children: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: FutureBuilder(
              future: client.getArticle(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Article>> snapshot) {
                if (snapshot.hasData) {
                  List<Article> articles = snapshot.data;
                  return ListView.builder(
                    itemCount: articles.length,
                    itemBuilder: (context, index) => ListTile(
                      title: customListTile(articles[index], context),
                    ),
                  );
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: FutureBuilder(
              future: client6.getArticle(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Article>> snapshot) {
                if (snapshot.hasData) {
                  List<Article> articles = snapshot.data;
                  return ListView.builder(
                    itemCount: articles.length,
                    itemBuilder: (context, index) => ListTile(
                      title: customListTile(articles[index], context),
                    ),
                  );
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: FutureBuilder(
              future: client2.getArticle(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Article>> snapshot) {
                if (snapshot.hasData) {
                  List<Article> articles = snapshot.data;
                  return ListView.builder(
                    itemCount: articles.length,
                    itemBuilder: (context, index) => ListTile(
                      title: customListTile(articles[index], context),
                    ),
                  );
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: FutureBuilder(
              future: client3.getArticle(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Article>> snapshot) {
                if (snapshot.hasData) {
                  List<Article> articles = snapshot.data;
                  return ListView.builder(
                    itemCount: articles.length,
                    itemBuilder: (context, index) => ListTile(
                      title: customListTile(articles[index], context),
                    ),
                  );
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: FutureBuilder(
              future: client4.getArticle(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Article>> snapshot) {
                if (snapshot.hasData) {
                  List<Article> articles = snapshot.data;
                  return ListView.builder(
                    itemCount: articles.length,
                    itemBuilder: (context, index) => ListTile(
                      title: customListTile(articles[index], context),
                    ),
                  );
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: FutureBuilder(
              future: client5.getArticle(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Article>> snapshot) {
                if (snapshot.hasData) {
                  List<Article> articles = snapshot.data;
                  return ListView.builder(
                    itemCount: articles.length,
                    itemBuilder: (context, index) => ListTile(
                      title: customListTile(articles[index], context),
                    ),
                  );
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
