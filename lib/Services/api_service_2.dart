import 'dart:convert';
import 'package:http/http.dart';
import 'package:sanber_news/Models/article_model.dart';

class ApiService2 {
  final endPointUrl =
      "https://newsapi.org/v2/top-headlines?country=id&category=entertainment&apiKey=30a3085dfea54609b4dfa04605e5c7d9";

  Future<List<Article>> getArticle() async {
    Response res = await get(Uri.parse(endPointUrl));

    if (res.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(res.body);

      List<dynamic> body = json['articles'];

      List<Article> articles =
          body.map((dynamic item) => Article.fromJson(item)).toList();

      return articles;
    } else {
      throw ("Can't get the Articles");
    }
  }
}
