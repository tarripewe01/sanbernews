import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sanber_news/HomeScreen.dart';
import 'package:sanber_news/Utils/Colors.dart';
import 'package:sanber_news/size_config.dart';

import 'SignInScreen.dart';
import 'main.dart';

class OnboardingScreen extends StatefulWidget {
  OnboardingScreen({Key key}) : super(key: key);

  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  int curretPage = 0;
  List<Map<String, String>> splashData = [
    {
      "text": "Welcome to sanberNews, \nLet's see the latest news today",
      "image": "assets/svg/sharing.svg"
    },
    {
      "text": "Trusted News & Business Navigator",
      "image": "assets/svg/onlinearticle.svg"
    },
    {
      "text": "Indonesia Dalam Satu Klik",
      "image": "assets/svg/worldismine.svg"
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myColors[PRIMARY],
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 3,
                child: PageView.builder(
                  onPageChanged: (value) {
                    setState(() {
                      curretPage = value;
                    });
                  },
                  itemCount: splashData.length,
                  itemBuilder: (context, index) => OnboardingContent(
                    text: splashData[index]["text"],
                    image: splashData[index]["image"],
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Column(
                    children: <Widget>[
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(splashData.length,
                            (index) => buildDot(index: index)),
                      ),
                      Spacer(flex: 3),
                      SizedBox(
                        width: double.infinity,
                        height: 56,
                        child: DefaultButton(
                          text: "Continue",
                          press: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        HomeScreen()));
                          },
                        ),
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
      duration: Duration(seconds: 1),
      margin: EdgeInsets.only(right: 5, top: 10),
      height: 6,
      width: curretPage == index ? 20 : 6,
      decoration: BoxDecoration(
          color: curretPage == index ? Colors.white : Color(0xFFD8D8D8),
          borderRadius: BorderRadius.circular(3)),
    );
  }
}

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key key,
    this.text,
    this.press,
  }) : super(key: key);
  final String text;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      color: Colors.white,
      onPressed: press,
      child: Text(
        text,
        style: TextStyle(
          fontSize: 18,
        ),
      ),
    );
  }
}

class OnboardingContent extends StatelessWidget {
  const OnboardingContent({
    Key key,
    this.text,
    this.image,
  }) : super(key: key);
  final String text, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Spacer(),
        Text.rich(
          TextSpan(
              text: "sanber",
              style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.w500,
                  color: Colors.black),
              children: [
                TextSpan(
                    text: "News",
                    style: TextStyle(
                        fontSize: 36,
                        fontWeight: FontWeight.bold,
                        color: Colors.white))
              ]),
        ),
        Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 16),
        ),
        Spacer(
          flex: 6,
        ),
        SvgPicture.asset(
          image,
          height: 265.0,
        ),
      ],
    );
  }
}

// Text.rich(
//             TextSpan(
//                 text: "sanber",
//                 style: TextStyle(
//                     fontSize: 24,
//                     fontWeight: FontWeight.w500,
//                     color: Colors.black),
//                 children: [
//                   TextSpan(
//                       text: "News",
//                       style: TextStyle(
//                           fontSize: 30,
//                           fontWeight: FontWeight.bold,
//                           color: Colors.white))
//                 ]),
//           ),
