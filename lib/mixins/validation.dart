import 'package:flutter/material.dart';

class Validation {
  String validateEmail(String value) {
    if (!value.contains('@')) {
      return 'Email tidak valid';
    }
    return null;
  }

  String validatePassword(String value) {
    if (value.length < 6) {
      return 'Password Minimal 6 Karakter';
    }
    return null;
  }
}
